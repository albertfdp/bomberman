import { Record } from 'immutable'

class Cell extends Record({
  id: undefined,
  type: 'wall'
}) { }

export default Cell
