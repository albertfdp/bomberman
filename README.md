# bomberman

[![Build Status](https://travis-ci.org/albertfdp/boilerplate.svg?branch=master)](https://travis-ci.org/albertfdp/boilerplate)

A state-of-the-art opinionated & very personal boilerplate. The stack includes:
  * Webpack 2
  * React
  * PostCSS && CSS Modules
  * Babel preset ENV -transpile just what you need-
  * Jest && unexpected.js

It's offline capable, so the app works without internet too!
